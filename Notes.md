1. create a new rails application with ruby 3.1.3 and rails 7.0.4.3
```
rails new rails_clickhouse
cd rails_clickhouse
```
2. start up a clickhouse database
(I used docker and docker-compose)
```
version: '3'
services:
  clickhouse:
    image: clickhouse/clickhouse-server:22.8.15.23
    ports:
      - 8123:8123
      - 9000:9000
```
4. add clickhouse-activrecord to the gem and bundle
```
bundle
```
5. configure config/database for standard development, test, production environments
```
default: &default
  adapter: clickhouse
  database: database
  host: clickhouse
  port: 8123

development:
  <<: *default

# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  <<: *default

production:
  <<: *default
```

6. run rails db:create
```
rails db:create
Created database 'database'
Response code: 500:
Code: 82. DB::Exception: Database database already exists. (DATABASE_ALREADY_EXISTS) (version 22.8.15.23 (official build))
Couldn't create 'database' database. Please check your configuration.
rails aborted!
ActiveRecord::ActiveRecordError: Response code: 500:
Code: 82. DB::Exception: Database database already exists. (DATABASE_ALREADY_EXISTS) (version 22.8.15.23 (official build))
```
7. run rails g scaffold hello
```
rails g scaffold hello message:string
      invoke  active_record
      create    db/migrate/20230417142726_create_hellos.rb
      create    app/models/hello.rb
      invoke    test_unit
      create      test/models/hello_test.rb
      create      test/fixtures/hellos.yml
      invoke  resource_route
       route    resources :hellos
      invoke  scaffold_controller
      create    app/controllers/hellos_controller.rb
      invoke    erb
      create      app/views/hellos
      create      app/views/hellos/index.html.erb
      create      app/views/hellos/edit.html.erb
      create      app/views/hellos/show.html.erb
      create      app/views/hellos/new.html.erb
      create      app/views/hellos/_form.html.erb
      create      app/views/hellos/_hello.html.erb
      invoke    resource_route
      invoke    test_unit
      create      test/controllers/hellos_controller_test.rb
      create      test/system/hellos_test.rb
      invoke    helper
      create      app/helpers/hellos_helper.rb
      invoke      test_unit
      invoke    jbuilder
      create      app/views/hellos/index.json.jbuilder
      create      app/views/hellos/show.json.jbuilder
      create      app/views/hellos/_hello.json.jbuilder
rails db:migrate
```
rails db:migrate did not do anything. I noticed that the default migration_path is db/clickhouse. This is not what a rails developer would expect to have to do, add configuration to conf/database.yml to tell it where its migrations are. Rails is all about convention over configuration. I configured the default migration_path to be db/migrate and reran. This created a new migration :)

8. Gotchas

- no autoincrement (not a problem for read-only source)
- no delete (not a problem for read-only source)
