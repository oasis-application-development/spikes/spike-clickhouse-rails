# Clickhouse-Rails application

## docker-compose

```
docker-compose up -d
docker-compose exec server rails db:create
docker-compose exec server rails db:migrate
```

db:create will appear to return an error, but it is actually successful

visit [local app](http://localhost:3000/hello)
