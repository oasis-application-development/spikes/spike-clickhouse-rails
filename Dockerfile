FROM ruby:3.1.3

ENV APP_PATH=/opt/app-root/src
ENV HOME=/opt/app-root

WORKDIR ${APP_PATH}

COPY Gemfile* .*-version *package* *yarn* ${APP_PATH}/

RUN curl -s https://gitlab.oit.duke.edu/oasis-application-development/developer-tools/-/raw/v1.5.1/ruby/polymath/polymath.sh | bash

COPY . ${APP_PATH}
RUN bundle install --retry 3 \
    && rm -rf `gem env gemdir`/cache/*.gem \
    && find `gem env gemdir`/gems/ -name "*.c" -delete \
    && find `gem env gemdir`/gems/ -name "*.o" -delete

RUN chmod -R g=rwX ${APP_PATH} ${HOME}
