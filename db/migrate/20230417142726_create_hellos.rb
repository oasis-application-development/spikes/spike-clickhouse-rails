class CreateHellos < ActiveRecord::Migration[7.0]
  def change
    create_table :hellos, id: false do |t|
      t.string :version
      t.string :message

      t.timestamps
    end
  end
end
