# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# clickhouse:schema:load`. When creating a new database, `rails clickhouse:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ClickhouseActiverecord::Schema.define(version: 2023_04_17_142726) do

  # TABLE: hellos
  # SQL: CREATE TABLE database.hellos ( `version` Nullable(String), `message` Nullable(String), `created_at` DateTime, `updated_at` DateTime ) ENGINE = Log
# Could not dump table "hellos" because of following StandardError
#   Unknown type 'Nullable(String)' for column 'version'

end
