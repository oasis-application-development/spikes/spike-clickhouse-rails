class Hello < ApplicationRecord
  self.primary_key = "version"

  def to_param
    version
  end
end
